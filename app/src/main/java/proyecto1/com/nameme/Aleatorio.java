package proyecto1.com.nameme;

import java.util.ArrayList;

public class Aleatorio implements Runnable {

    private ArrayList<Silaba> silabas = new ArrayList<>();
    private StringBuilder salidaKanji = new StringBuilder("");;
    private StringBuilder salidaRomanji = new StringBuilder("");;

    public Aleatorio(ArrayList<Silaba> silabas) {
        this.silabas = silabas;
    }

    public StringBuilder getSalidaKanji() {
        return salidaKanji;
    }

    public StringBuilder getSalidaRomanji() {
        return salidaRomanji;
    }

//    public void generarNombre()
//    {
//    }
    @Override
    public void run() {

        int random;
        int i = 0;
        int longitudNombre = ((int) (Math.random() * 2)) + 2;

        do
        {
            StringBuilder kanjiChar = new StringBuilder("");
            StringBuilder romanjiChar = new StringBuilder("");
            random = (int) (Math.random() * silabas.size());

            if(silabas.get(random).getRepeticionesMax() != 0)
            {
                kanjiChar = new StringBuilder(silabas.get(random).getKanji());
                romanjiChar = new StringBuilder(silabas.get(random).getRomanji());

                if (romanjiChar.equals(new StringBuilder("n")) && i == 0) {
                    continue;
                }

                silabas.get(random).setRepeticionesMax(silabas.get(random).getRepeticionesMax() - 1);
                i++;
            }
            salidaKanji.append(kanjiChar);
            salidaRomanji.append(romanjiChar);

        }
        while(i < longitudNombre);

    }
}
