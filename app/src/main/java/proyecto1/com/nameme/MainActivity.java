package proyecto1.com.nameme;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Silaba> silabas;
    private StringBuilder nombreKanji = new StringBuilder("");
    private StringBuilder nombreRomanji = new StringBuilder("");
    private SharedPreferences preferencias;
    private final String KANJI = "nombreKanji";
    private final String ROMANJI = "nombreRomanji";
    private Integer orientacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ventana_principal);
        nombreKanji = new StringBuilder("");
        nombreRomanji = new StringBuilder("");
        preferencias = getPreferences(MODE_PRIVATE);
        orientacion = getResources().getConfiguration().orientation;

        // Rellenenamos la lista de silabas
        silabas = new ArrayList<>();
        Resources res = this.getResources();
        String []cadena = res.getStringArray(R.array.silabas);

        for (String s : cadena){
            Silaba silaba = new Silaba();
            StringTokenizer st = new StringTokenizer(s);
            silaba.setKanji(st.nextToken());
            silaba.setRomanji(st.nextToken());
            silaba.setRepeticionesMax(Integer.parseInt(st.nextToken()));
            System.out.println(silaba.toString());
            silabas.add(silaba);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        int oriant = preferencias.getInt("orientacion", 5);
        if (oriant != 5 && oriant != orientacion){
            nombreKanji = new StringBuilder(preferencias.getString(KANJI, ""));
            nombreRomanji = new StringBuilder(preferencias.getString(ROMANJI, ""));
            rellenarCampos();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString(KANJI, nombreKanji.toString());
        editor.putString(ROMANJI, nombreRomanji.toString());
        editor.putInt("orientacion", orientacion);
        editor.apply();
        editor.commit();
    }

    public void onClick(View v){
        Aleatorio nombre = new Aleatorio (silabas);
        nombre.run();

        nombreKanji = nombre.getSalidaKanji();
        nombreRomanji = nombre.getSalidaRomanji();

        rellenarCampos();
    }

    private void rellenarCampos (){
        TextView textoKanji = findViewById(R.id.TextKanji);
        textoKanji.setText(nombreKanji);
        TextView textoromanji = findViewById(R.id.TextRomanji);
        textoromanji.setText(nombreRomanji);
    }
}
