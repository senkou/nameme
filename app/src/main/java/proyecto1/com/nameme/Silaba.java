package proyecto1.com.nameme;

/**
 * Created by mruizmor on 20/02/2018.
 */

public class Silaba {

    String Kanji;
    String romanji;
    int repeticionesMax;

    public Silaba() {
    }

    public Silaba(String kanji, String romanji, int repeticionesMax) {
        Kanji = kanji;
        this.romanji = romanji;
        this.repeticionesMax = repeticionesMax;
    }

    public String getKanji() {
        return Kanji;
    }

    public void setKanji(String kanji) {
        Kanji = kanji;
    }

    public String getRomanji() {
        return romanji;
    }

    public void setRomanji(String romanji) {
        this.romanji = romanji;
    }

    public int getRepeticionesMax() {
        return repeticionesMax;
    }

    public void setRepeticionesMax(int repeticionesMax) {
        this.repeticionesMax = repeticionesMax;
    }
}
